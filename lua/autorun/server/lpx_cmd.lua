include("lpx_treasure_main.lua")
include("lpx_treasure_db.lua")

concommand.Add("lpx_treasure_addspawn", function(ply)
	if ply:IsUserGroup("superadmin") then
		local pos = ply:GetPos()
		table.insert(ArtSpawns, pos)
		SaveSpawn(pos)
	end
end
)

concommand.Add("lpx_treasure_removespawn", function(ply)
	if ply:IsUserGroup("superadmin") then
		local pos = ply:GetPos()
		for i=1, #ArtSpawns do
			if ArtSpawns[i]:Distance(pos) < 50 then
				DeleteSpawn(ArtSpawns[i])
				table.remove(ArtSpawns, i)
			end
		end
	end
end
)