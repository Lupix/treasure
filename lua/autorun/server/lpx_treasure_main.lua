include("lpx_treasure_db.lua")
include("lpx_treasure_cfg.lua")

Artefacts = Artefacts or {}
ArtSpawns = ArtSpawns or {}

RARITY_EPIC, RARITY_HEROIC, RARITY_LEGENDARY = 1, 2, 3

function AddArtefact(artName, artModel, artRarity, artPrice, artLore, artLoreSound)
	local artefact = {Name = artName, Model = artModel, Rarity = artRarity, Price = artPrice, Lore = artLore, LoreSpeech = artLoreSound}
	table.insert(Artefacts, artefact)
	print("[LPX TREASURE] Artefact '".. artefact["Name"] .."' has been successfully registered.")
end


-- when the addon gets loaded
local function OnAddonInit()
	TableInit()
	LoadSpawns()
end

hook.Add("Initialize", "AddonInit", OnAddonInit)

hook.Add("OnPlayerChangedTeam", "ChangeTeam", function(ply, oldTeam, newTeam)
	if newTeam == TEAM_TREASUREHUNTER then
		if team.NumPlayers(TEAM_TREASUREHUNTER) == 1 then
			local artefact = ents.Create("lpx_artefact")
			artefact:SetPos(table.Random(ArtSpawns))
			artefact:Spawn()
			ply:PrintMessage("There are rumours that deep in the sewers, the Sewer People are guarding something very valuable. You should go look for it.")
		end
	end
end
)