include("lpx_treasure_main.lua")
-- Add Artefacts under this line
-- EXAMPLE: AddArtefact("Artefact Name", "path/to/model.mdl", RARITY_HEROIC, 1000, "This item is the most default item in the world")

AddArtefact("Book of Necromancy", "none", RARITY_HEROIC, 10750, "no lore", "none")

AddArtefact("Kronodin's Helm of Decay", "none", RARITY_LEGENDARY, 14500, "no lore", "none")

AddArtefact("Gem of Perception", "none", RARITY_EPIC, 5250, "no lore", "none")

AddArtefact("Pendant of Dexterity", "none", RARITY_EPIC, 4500, "no lore", "none")

AddArtefact("Staff of Heretics", "none", RARITY_EPIC, 6000, "no lore", "none")

AddArtefact("Gloves of Reaper's Touch", "none", RARITY_HEROIC, 9500, "no lore", "none")