net.Receive("lpx_treasure_reward", function(len, ply)
	local DReward
	local DArtNameLbl
	local DArtRarityLbl
	local DArtModelView
	local DArtLoreLbl
	local DClaimBtn
	local DRewardLbl

	local ArtInfo = net.ReadTable()
	local rarity = ArtInfo["Rarity"]

	if rarity == 1 then rarity = "Epic"
	elseif rarity == 2 then rarity = "Heroic"
	elseif rarity == 3 then rarity = "Legendary"
	end

	local loreMusic = CreateSound("lpx_treasure/lore.mp3")
	loreMusic:Play()
	loreMusic:ChangeVolume(0.5)

	local loreSpeech = CreateSound(ArtInfo["LoreSpeech"])
	loreSpeech:Play()

	DReward = vgui.Create("DFrame")
	DReward:SetSize(ScrW() * 0,4, ScrH() * 0,75)
	DReward:Center()
	DReward:SetBackgroundBlur(true)
	DReward:ShowCloseButton(false)

	DArtNameLbl = vgui.Create("DLabel", DReward)
	DArtNameLbl:SetText(ArtInfo["Name"])
	DArtNameLbl:SetColor(Color(102, 0, 102))
	DArtNameLbl:Dock(TOP)

	DArtRarityLbl = vgui.Create("DLabel", DReward)
	DArtRarityLbl:SetText(rarity)
	DArtRarityLbl:SetColor(Color(255, 255, 102))
	DArtRarityLbl:Dock(TOP)

	DArtModelView = vgui.Create("SpawnIcon", DReward)
	DArtModelView:Dock(TOP)
	DArtModelView:SetModel(ArtInfo["Model"])

	DArtLoreLbl = vgui.Create("DLabel", DReward)
	DArtLoreLbl:SetText(ArtInfo["Lore"])
	DArtLoreLbl:Dock(FILL)
	DArtLoreLbl:SetColor(Color(255, 255, 255))

	DClaimBtn = vgui.Create("DButton", DReward)
	DClaimBtn:SetText("Claim Reward")
	DClaimBtn:Dock(BOTTOM)
	DClaimBtn.DoClick = function()
		net.Start("lpx_treasure_reward_claim")
		net.WriteInt(ArtInfo["Price"], 32)
		net.SendToServer()
		DReward:Close()
		loreMusic:Stop()
		loreSpeech:Stop()
	end

	DRewardLabel = vgui.Create("DLabel", DReward)
	DRewardLabel:SetText(ArtInfo["Price"])
	DRewardLabel:SetColor(Color(0, 255, 0))
	DRewardLabel:Dock(BOTTOM)

end
)	