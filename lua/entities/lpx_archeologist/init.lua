util.AddNetworkString("lpx_treasure_reward")
util.AddNetworkString("lpx_treasure_reward_claim")

net.Receive("lpx_treasure_reward_claim", function(len, ply)
	ply:addMoney(net.ReadInt(32))
end
)

function ENT:Initialize()
	self:SetModel("models/Humans/Group01/Female_01.mdl")

	self:SetHullType(HULL_HUMAN)
	self:SetHullSizeNormal()

	self:SetSolid(SOLID_BBOX) 
	self:SetMoveType(MOVETYPE_NONE)

	self:SetUseType(SIMPLE_USE)

	self:CapabilitiesAdd(CAP_ANIMATEDFACE)
end

function ENT:Use(activator, caller, useType, value)
	if activator:Team() == TEAM_TREASUREHUNTER and activator.ArtInfo then
		net.Start("lpx_treasure_reward")
		net.WriteTable(activator.ArtInfo)
		net.Send()
		activator.ArtInfo = nil
	end
end