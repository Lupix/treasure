util.AddNetworkString("lpx_treasure_found")

AddCSLuaFile("shared.lua")
include("shared.lua")

include("autorun/server/lpx_treasure_main.lua")
include("autorun/server/lpx_treasure_cfg.lua")

ENT.ArtInfo = ENT.ArtInfo or {}

function ENT:Initialize()
	local cnt = 1
	local idx = math.random(#Artefacts)
	
	while Artefacts[idx]["Rarity"] > cnt then
		idx = math.random(#Artefacts)
		cnt = cnt + 1
	end

	self.ArtInfo = Artefacts[idx]
	self:SetModel(self.ArtInfo["Model"])
	self:SetMoveType(MOVETYPE_NONE)
end

function ENT:PhysicsCollide(colData, collider)
	local ent = colData.HitEntity
	if ent:IsPlayer() and ent:Team() == TEAM_TREASUREHUNTER then
		if not ent.ArtInfo then
			ent.ArtInfo = self.ArtInfo
			net.Start("lpx_treasure_found")
			net.WriteString(self.ArtInfo["Name"])
			net.Send(ent)
			self:Remove()
			print(ent:Nick() .. " has picked up artefact " .. ent.ArtInfo["Name"])
			timer.Simple(TREASURE_SPAWN_DELAY, function()
				if team.GetPlayers(TEAM_TREASUREHUNTER) == 0 then return end
				local artefact = ents.Create("lpx_artefact")
				artefact:SetPos(table.Random(ArtSpawns))
				artefact:Spawn()
				ply:PrintMessage("There are rumours that deep in the sewers, the Sewer People are guarding something very valuable. You should go look for it.")
			end
			)
		end
	end
end