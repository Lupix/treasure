function TableInit()
	if not sql.TableExists("lpx_treasure_spawn") then
		sql.Query("CREATE TABLE lpx_treasure_spawn (Pos_X real, Pos_Y real, Pos_Z real)")
	end
end

function SaveSpawn(vectorPos)
	sql.Query("INSERT INTO lpx_treasure_spawn VALUES (".. vectorPos["x"] ..", ".. vectorPos["y"] .. ", ".. vectorPos["z"] ..")")
end

function LoadSpawns()
	local res = sql.Query("SELECT * FROM lpx_treasure_spawn")
	if res then
		local ArtSpawn = Vector(tonumber(res["Pos_X"]), tonumber(res["Pos_Y"]), tonumber(res["Pos_Z"]))
		table.insert(ArtSpawns, ArtSpawn)
	end
end

function DeleteSpawn(vectorPos)
	sql.Query("DELETE FROM lpx_treasure_spawn WHERE Pos_X=".. vectorPos["x"] .. " AND Pos_Y=".. vectorPos["y"] .. " AND Pos_Z=".. vectorPos["z"] .. ")")
end